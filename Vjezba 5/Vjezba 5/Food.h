#pragma once
#ifndef FOOD_H
#define FOOD_H
#include <iostream>
#include <string>
#include "Evidencija.h"
using namespace std;

struct Datum {
	int dan;
	int mjesec;
	int godina;
};
class Potrosnja {
	int godina;
	int mjesec;
	int mjesecna_potrosnja;
public:
	Potrosnja() {this->mjesec = 0, this->mjesecna_potrosnja = 0; this->godina = 0;};
	Potrosnja(int mjesec, int mjesecna_potrosnja, int godina) {this->mjesec = mjesec, this->mjesecna_potrosnja = mjesecna_potrosnja, this->godina = godina;};
	int potrosnja_mjesec() const {return mjesec;};
	int potrosnja_godina() const {return godina;};
	int potrosnja_mjesceno() const {return mjesecna_potrosnja;};
	void printaj() const;

	void postavi_mjesecno(int kg) {this->mjesecna_potrosnja = kg;};
	void postavi_mjesec(int mje) {this->mje = mje;};
	void postavi_godinu(int god) {this->god = god;};
};
class Food {
	string vrsta, naziv;
	Datum datum;
	int velicina;
	int dnevna_kol;
	int kolicina_vode, kolicina_proteina, kolicinu_masti, kolicina_ugljikohidrata;
	Potrosnja *potrosnja;
public:
	Food(string v, string n, Datum rok, int dnevna_kolicina, int voda, int proteini, int masti, int ugljik);
	~Food();
	Food(const Food &f);
	void promjeni_kolicinu(bool x);
	void postavi_potrosnju(int kg, unsigned mje, unsigned god);
	double postotak() const;
	int dohvati_mjesec() const;
	int dohvati_godinu() const;
	void print() const;
/*
class Food
{
	std::string ovrsta, onaziv,orok_trajanja;
	double okol_vode, okol_proteina, okol_masti, okol_ugljikohidrata, odnevna_potreba;
	Evidencija *potrosnja;
public:
	Food();
	Food(std::string vrsta, std::string naziv, std::string rok_trajanja, double kol_vode, double kol_proteina, double kol_masti, double kol_ugljikohidrata, double dnevna_potreba);
	~Food();
	double Promjena_Potrebe(double dnevna_potreba, char c);
	double Potrosnja_Status();

};
*/
#endif