#pragma once
#include "Point.h"
#ifndef TARGET_H
#define TARGET_H

class Target
{
	Point polozaj;
	double visina;
	double sirina;
	bool status=false;
public:
	void PostaviVrijednost(Point& a, double v, double s);
	void IspisiVrijednost();
	void ProvjeraStatusa(Point& a);
};
#endif //PCH_H

