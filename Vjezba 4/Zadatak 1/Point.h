#pragma once
#ifndef POINT_H
#define POINT_H

class Point
{
	double x;
	double y;
	double z;
	friend class Gun;
	friend class Target;
public:
	void PostaviVrijednost(double a = 0, double b = 0, double c = 0);
	void PostaviRandom(int a, int b);
	void IspisiVrijednost();
	double DvaDUdaljenost(Point& tocka);
	double TriDUdaljenost(Point& tocka);

};
#endif //PCH_H
