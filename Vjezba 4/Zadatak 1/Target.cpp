#include "Target.h"
#include <iostream>
void Target::PostaviVrijednost(Point& a, double v, double s)
{
	polozaj.PostaviVrijednost(a.x, a.y, a.z);
	visina = v;
	sirina = s;
}
void Target::IspisiVrijednost()
{
	std::cout << "Koordinate mete su ";
	polozaj.IspisiVrijednost();
	std::cout << "Visina je " << visina << " a sirina je " << sirina << std::endl;
	if (status == false)
		std::cout << "Meta je nepogodena" << std::endl;
	else
		std::cout << "Meta je pogodena" << std::endl;
}
void Target::ProvjeraStatusa(Point& a)
{
	if (a.z >= polozaj.z && a.z <= (polozaj.z + visina))
		status = true;
}
