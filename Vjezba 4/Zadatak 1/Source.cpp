#include "Point.h"
#include "Gun.h"
#include "Target.h"
#include <time.h>
#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main()
{
	srand(time(NULL));
	int x,n,i,capacity;
	double a, b, c, v,s;
	string str;
	vector<Target> vektor;
	Point tocka, tocka2, gun, target, temp;
	Gun puska;
	Target meta;
	cout << "Unesite broj meta"<<endl;
	cin >> n;
	cout << "Zelite li unijeti podatke o pusci (Y/N)" << endl;
	cin.ignore(1000, '\n');
	getline(cin, str);
	while (str != "y" && str != "Y" && str != "N" && str != "n")
	{
		cout << "Error" << endl;
		getline(cin, str);
	}
	if (str == "Y" || str == "y")
	{
		cout << "Unesite x koordinatu puske" << endl;
		cin >> a;
		cout << "Unesite y koordinatu puske" << endl;
		cin >> b;
		cout << "Unesite z koordinatu puske" << endl;
		cin >> c;
		cout << "Unesite kapacitet puske" << endl;
		cin >> capacity;
		gun.PostaviVrijednost(a, b, c);
		puska.PostaviVrijednost(gun, capacity);
		puska.IspisiVrijednost();
	}
	else
	{
		gun.PostaviRandom(0, 10);
		puska.PostaviVrijednost(gun, 4);
		puska.IspisiVrijednost();
	}
	

	for (i = 0; i < n; i++)
	{
		vektor.push_back(Target());
		cout << "Unesite visinu mete" << endl;
		cin >> v; 
		cout << "Unesite sirinu mete" << endl;
		cin >> s;
		cout << "Zelite li unijeti koordinate mete (Y/N)";
		getline(cin, str);
		while (str != "y" && str != "Y" && str != "N" && str != "n")
		{
			cout << "Error" << endl;
			getline(cin, str);
		}
		if (str == "Y" || str == "y")
		{
			cout << "Unesite x koordinatu mete" << endl;
			cin >> a;
			cout << "Unesite y koordinatu mete" << endl;
			cin >> b;
			cout << "Unesite z koordinatu mete" << endl;
			cin >> c;
			temp.PostaviVrijednost(a, b, c);
		}
		else 
		{
			temp.PostaviRandom(0, 10);
		}
		vektor.at(i).PostaviVrijednost(temp, v, s);
		puska.Shoot();
		vektor.at(i).ProvjeraStatusa(gun);
		vektor.at(i).IspisiVrijednost();
	}
	tocka.PostaviVrijednost(5,4,3);
	tocka.IspisiVrijednost();
	tocka2.PostaviRandom(0, 10);
	tocka2.IspisiVrijednost();
	cout << "2D udaljenost je: " << tocka.DvaDUdaljenost(tocka2)<< endl;
	cout << "3D udaljenost je: " << tocka.TriDUdaljenost(tocka2) << endl;
	std::cout << "Hello World!\n";
	cout << "Bye world! *shoots*" << endl;
	cin >> x;
}
