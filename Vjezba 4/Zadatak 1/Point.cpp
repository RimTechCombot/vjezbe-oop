#include "Point.h"
#include <iostream>

void Point::PostaviVrijednost(double a, double b, double c)
{
	x = a;
	y = b;
	z = c;
}
void Point::PostaviRandom(int a, int b)
{
	x= rand() % (b - a + 1) + a;
	y= rand() % (b - a + 1) + a;
	z= rand() % (b - a + 1) + a;
}
void Point::IspisiVrijednost()
{
	std::cout << x << "  " << y << "  " << z << "  " << std::endl;
}
double Point::DvaDUdaljenost(Point& tocka)
{
	return (tocka.x - x)*(tocka.x - x) + (tocka.y - y)*(tocka.y - y);
}
double Point::TriDUdaljenost(Point& tocka)
{
	return (tocka.x - x)*(tocka.x - x) + (tocka.y - y)*(tocka.y - y) + (tocka.z - z)*(tocka.z - z);
}