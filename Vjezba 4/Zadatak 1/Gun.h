#pragma once
#ifndef GUN_H
#define GUN_H
#include "Point.h"
class Gun
{
	Point polozaj;
	int total;
	int current;
	friend class Point;
public:
	void setPolozajRandom();
	void IspisiVrijednost();
	void PostaviVrijednost(Point& a,int totalm);
	void Shoot();
	void Reload();
};
#endif //PCH_H
