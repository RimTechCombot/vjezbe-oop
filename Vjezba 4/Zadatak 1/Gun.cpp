#include "Gun.h"
#include <iostream>
#include <windows.h>
void Gun::Reload()
{
	current = total;
	std::cout << "Napunili ste metke\n";
}
void Gun::Shoot()
{
	current--;
	if (current > 1) std::cout << "Ostala su jos " << current << " metka" << std::endl;
	else if (current == 0) 
	{
		std::cout << "Ostali ste bez metaka... punjenje" << std::endl; 
		Sleep(1000);
		std::cout << "..." << std::endl;
		Sleep(1000);
		std::cout << "..." << std::endl;
		Sleep(1000);
		std::cout << "..." << std::endl;
		Sleep(1000);
	}
	else std::cout << "Ostao je jos 1 metak" << std::endl;
	if (current == 0) Gun::Reload();
}
void Gun::PostaviVrijednost(Point& a, int totalm)
{
	polozaj.PostaviVrijednost(a.x, a.y, a.z);
	total = totalm;
	current = totalm;
}
void Gun::IspisiVrijednost()
{
	std::cout << "Koordinate puske su ";
	polozaj.IspisiVrijednost();
	std::cout << "Kapcitet puske je " << total << " a trenutni broj metaka " << current << std::endl;
}
