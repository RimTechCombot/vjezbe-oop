#ifndef INTERFACE_H
#define INTERFACE_H
#include <iostream>
#include <string>
using namespace std;
class Interface
{
public:
	virtual void type() = 0;
	virtual void platform() = 0;
};
#endif