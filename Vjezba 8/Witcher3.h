#ifndef WITCHER3_H
#define WITCHER3_H
#include <iostream>
#include <string>
#include "RPG.h"
#include "OpenWorld.h"
using namespace std;
class Witcher3 : public RPG, public OpenWorld
{
protected:
	string platform;
	string type;
public:
	Witcher3() { type = "OpenWorld RPG"; };
	~Witcher3() { cout << "destructor" << endl; };
	string gettype() { return type; };
	string getplatform() { return platform; };
	void setplatform(string buffer) { platform = buffer; };

};
#endif