#ifndef OPENWORLD_H
#define OPENWORLD_H
#include <iostream>
#include <string>
#include "VideoGame.h"
using namespace std;
class OpenWorld: virtual public VideoGame
{
protected:
	string type;
public:
	OpenWorld() { type = "OpenWorld"; cout << "OpenWorldConst" << endl; };
	string gettype() { return type; };
	string getplatform() = 0;
};
#endif