#ifndef DARKSOULS_H
#define DARKSOULS_H
#include <iostream>
#include <string>
#include "RPG.h"
using namespace std;
class DarkSouls : public RPG
{
protected:
	string platform="XBOX";
public:
	DarkSouls() {};
	string gettype() { return type; };
	string getplatform() { return platform; };
	void setplatform(string buffer) { platform = buffer; };
};
#endif
