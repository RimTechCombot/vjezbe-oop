#ifndef COUNTER_H
#define COUNTER_H
#include <iostream>
#include <string>
#include "VideoGame.h"
using namespace std;
namespace OSS
{ 
class Counter
{
protected:
	int PC;
	int XBOX;
	int PS4;
public:
	Counter() { PC = 0; XBOX = 0; PS4 = 0; }
	void countplatforms(VideoGame* p);
	void getmostfrequent();
};
}
#endif