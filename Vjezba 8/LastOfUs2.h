#ifndef LASTOFUS2_H
#define LASTOFUS2_H
#include <iostream>
#include <string>
#include "Action.h"
using namespace std;
class LastOfUs2 : public Action
{
protected:
	string platform;
public:
	LastOfUs2() {};
	string gettype() { return type; };
	string getplatform() { return platform; };
	void setplatform(string buffer) { platform = buffer; };
};
#endif