#ifndef GODOFWAR_H
#define GODOFWAR_H
#include <iostream>
#include <string>
#include "Action.h"
using namespace std;
class GodOfWar : public Action
{
protected:
	string platform;
public:
	GodOfWar() {};
	~GodOfWar() { cout << "destructor" << endl; };
	string gettype() { return type; };
	string getplatform() { return platform; };
	void setplatform(string buffer) { platform = buffer; };

};
#endif