#ifndef RPG_H
#define RPG_H
#include <iostream>
#include <string>
#include "VideoGame.h"
using namespace std;
class RPG : virtual public VideoGame
{
protected:
	string type;
public:
	RPG() { type = "RPG"; cout << "RPGConst" << endl; };
	string gettype() { return type; };
	string getplatform() = 0;
};
#endif
