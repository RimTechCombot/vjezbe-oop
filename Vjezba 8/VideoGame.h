#ifndef VIDEOGAME_H
#define VIDEOGAME_H
#include <iostream>
#include <string>
using namespace std;
class VideoGame
{
protected:
public:
	virtual string gettype() = 0;
	virtual string getplatform() = 0;
	virtual void setplatform(string buffer) = 0;
};
#endif