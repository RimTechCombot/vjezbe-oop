#ifndef ACTION_H
#define ACTION_H
#include <iostream>
#include <string>
#include "VideoGame.h"
using namespace std;
class Action : public VideoGame
{
protected:
	string type;
public:
	Action() { type = "Action"; cout << "ActionConst" << endl; };
	string gettype() { return type; };
	
};
#endif