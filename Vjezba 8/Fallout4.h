#ifndef FALLOUT4_H
#define FALLOUT4_H
#include <iostream>
#include <string>
#include "RPG.h"
using namespace std;
class Fallout4 : public RPG
{
protected:
	string platform;
public:
	Fallout4() {};
	~Fallout4() { cout << "destructor" << endl; };
	string gettype() { return type; };
	string getplatform() { return platform; };
	void setplatform(string buffer) { platform = buffer; };

};
#endif