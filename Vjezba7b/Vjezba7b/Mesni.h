#pragma once
#ifndef MESNI_H
#define MESNI_H
#include <iostream>
#include <string>
#include "Food.h"
class Mesni : public Food
{
	Mesni(std::string vrsta, std::string naziv, std::string rok_trajanja, double kol_vode, double kol_proteina, double kol_masti, double kol_ugljikohidrata, double dnevna_potreba);
	~Mesni() { std::cout << "I am become destructor, destroyer of objects"; };
protected:
	double potrosnja_drugo, potrosnja_samostalno;
};
#endif