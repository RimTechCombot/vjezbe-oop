#pragma once
#ifndef VEGE_H
#define VEGE_H
#include <iostream>
#include <string>
#include "Food.h"
class Vege : public Food
{

	Vege(std::string vrsta, std::string naziv, std::string rok_trajanja, double kol_vode, double kol_proteina, double kol_masti, double kol_ugljikohidrata, double dnevna_potreba);
	~Vege() { std::cout << "I am become destructor, destroyer of objects"; };
protected:
	double potrosnja_prilog, potrosnja_samostalno;
};
#endif