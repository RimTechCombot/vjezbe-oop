#pragma once
#ifndef KOLACI_H
#define KOLACI_H
#include <iostream>
#include <string>
#include "Food.h"
class Kolaci : public Food
{

	Kolaci(std::string vrsta, std::string naziv, std::string rok_trajanja, double kol_vode, double kol_proteina, double kol_masti, double kol_ugljikohidrata, double dnevna_potreba);
	~Kolaci() { std::cout << "I am become destructor, destroyer of objects"; };
protected:
	double potrosnja_desert;
};
#endif