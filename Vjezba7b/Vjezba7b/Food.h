
#pragma once
#ifndef FOOD_H
#define FOOD_H
#include <iostream>
#include <string>
#include "Evidencija.h"
class Food
{
protected:
	std::string ovrsta, onaziv, orok_trajanja;
	double okol_vode, okol_proteina, okol_masti, okol_ugljikohidrata, odnevna_potreba;
	Evidencija *potrosnja;
public:
	Food();
	Food(std::string vrsta, std::string naziv, std::string rok_trajanja, double kol_vode, double kol_proteina, double kol_masti, double kol_ugljikohidrata, double dnevna_potreba);
	~Food();
	double Promjena_Potrebe(double dnevna_potreba, char c);
	double Potrosnja_Status();

};
#endif