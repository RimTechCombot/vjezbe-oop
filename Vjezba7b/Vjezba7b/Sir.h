#pragma once
#ifndef SIR_H
#define SIR_H
#include <iostream>
#include <string>
#include "Mlijecni.h"
using namespace std;
class Sir : public Mlijecni
{
protected:
	string oime;
public:
	Sir() { cout << "default constructor" << endl; };
	Sir(std::string vrsta, std::string naziv, std::string rok_trajanja, std::string ime,
		double kol_vode,
		double kol_proteina,
		double kol_masti,
		double kol_ugljikohidrata,
		double dnevna_potreba,
		double potrosnja_drugo,
		double potrosnja_samostalno);
	friend istream& operator>>(istream& is, Sir& s);
	friend ostream& operator<<(ostream& os, const Sir& s);
	~Sir() { std::cout << "I am become destructor, destroyer of objects" << endl; };
};
#endif
