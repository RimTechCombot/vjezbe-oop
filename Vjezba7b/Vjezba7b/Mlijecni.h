#pragma once
#ifndef MLIJECNI_H
#define MLIJECNI_H
#include <iostream>
#include <string>
#include "Food.h"
using namespace std;
class Mlijecni : public Food
{
protected:
	double opotrosnja_drugo, opotrosnja_samostalno;
public:
	Mlijecni() { cout << "default constructor"<< endl; }
	Mlijecni(std::string vrsta, std::string naziv, std::string rok_trajanja,
		double kol_vode,
		double kol_proteina,
		double kol_masti,
		double kol_ugljikohidrata,
		double dnevna_potreba,
		double potrosnja_drugo,
		double potrosnja_samostalno);
	~Mlijecni() { std::cout << "I am become destructor, destroyer of objects"<< endl; };
};
#endif