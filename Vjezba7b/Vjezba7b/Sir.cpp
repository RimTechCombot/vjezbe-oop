#include<iostream>
#include<string>
#include"Mlijecni.h"
#include"Sir.h"
using namespace std;
Sir::Sir(std::string vrsta,
	std::string ime,
	std::string naziv,
	std::string datum,
	double kol_vode,
	double kol_proteina,
	double kol_masti,
	double kol_ugljikohidrata,
	double dnevna_potreba,
	double potrosnja_drugo,
	double potrosnja_samostalno):
Mlijecni(vrsta, naziv, datum, kol_vode, kol_proteina, kol_masti, kol_ugljikohidrata, dnevna_potreba, potrosnja_drugo, potrosnja_samostalno)
{
	oime = ime;
	vrsta = "Mlijecni";
}
istream& operator>>(istream & is, Sir & s) {
	int choice;
	cout << "Unesite ime sira";
	is >> s.oime;
	cout << "Unesite kolicinu vode, proteina, masti, ugljikohidrata u 100 grama (tim redom)" << endl;
	is >> s.okol_vode >> s.okol_proteina >> s.okol_masti >> s.okol_ugljikohidrata;
	cout << "Samostalno(1) ili kao dio drugog jela?(2)";
	cin >> choice;
	while (choice != 1 && choice != 2)
	{
		cout << "Krivi input";
		cin >> choice;
	}
	if (choice == 1)
	{
		is >> s.opotrosnja_samostalno;
		s.opotrosnja_drugo = 0;
	}
	else
	{
		is >> s.opotrosnja_drugo;
		s.opotrosnja_samostalno = 0;
	}
	cout << "Unesite trenutnu dnevnu potrebu za tom hranom";
	cin >> s.odnevna_potreba;
	cout << "Unesite krajnji rok trajanja(DD-MM-YYYY";
	cin >> s.orok_trajanja;
	return is;

}

ostream& operator<<(ostream & os, const Sir & s) {
	os << s.oime << "\t" << s.okol_masti <<"\t" <<s.okol_proteina << "\t" << s.okol_vode << "\t" << s.okol_ugljikohidrata << "\t" << s.opotrosnja_samostalno << "\t" <<s.odnevna_potreba << endl;
	return os;}
