#include<iostream>
#include<string>
#include"Food.h"
#include"Mlijecni.h"
Mlijecni::Mlijecni(std::string vrsta, 
	std::string naziv, 
	std::string datum, 
	double kol_vode, 
	double kol_proteina, 
	double kol_masti, 
	double kol_ugljikohidrata, 
	double dnevna_potreba, 
	double potrosnja_drugo, 
	double potrosnja_samostalno):
Food(vrsta, naziv, datum,kol_vode, kol_proteina, kol_masti, kol_ugljikohidrata, dnevna_potreba)
{
	opotrosnja_drugo = potrosnja_drugo;
	opotrosnja_samostalno = potrosnja_samostalno;
}
