#pragma once
/*
Napisi implementaciju za template klasu Stack po LIFO algoritmu tako da :
� klasa sadrzi konstante int defaultSize u vrijednosti od 10 i maxSize u vrijednosti
od 1000,
� int clanove size i top
� template pointer stackPtr;
� napisati konstruktor s defaultnim parametrom i destruktor
� template metode push i pop
� pomocne metode za provjeru je li stack popunjen ili prazan
� pomocne metode za dohvat top elementa i velicine stacka
*/
#ifndef STACK_H
#define STACK_H
#include <iostream>
#define DEFAULTSIZE 10
#define MAXSIZE 1000
template <typename T>
class Stack
{
private:
	int size;
	int top;
	T* stackPtr;
public:
	Stack(int size=DEFAULTSIZE) { 
		if (size > MAXSIZE) { cout << "Max size is 1000 and has been set accordingly" << endl; this->size = MAXSIZE; }
		else { this->size = size; }
		stackPtr = new T[size];
		top = -1;
		cout << "Job's done, milord" << endl;
	};
	~Stack() { 
		delete[] stackPtr;
		cout << "Our town is under siege" << endl; 
	};
	int getSize() { return size; };
	int getTop() { return top; };

	bool stackFull() { return size - 1 == top; };
	bool stackEmpty() { return top == -1; };
	void push(T element) {
		if (stackFull()) {cout << "Stack is full cannot add element" << endl; return;}
		stackPtr[++top] = element;
	};
	T pop() {
		if (stackEmpty()) { cout << "Stack is empty cannot remove element" << endl; return 0; }
		return stackPtr[top--];
	};
};


#endif