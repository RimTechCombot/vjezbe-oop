#pragma
#ifndef ZBROJ_H
#define ZBROJ_H
#include <iostream>
using namespace std;
template <typename T>
class Zbroj
{
private:
	T a, b;
public:
	Zbroj() { cout << "Pozvan basic b*tch constructor" << endl; };
	void set_a(T a) { this->a = a; }
	void set_b(T b) { this->b = b; }
	T get_a() { return a; };
	T get_b() { return b; };
	T zbroji(T a,T b){ return (a + b); };
};

template<>
class Zbroj<char>
{
private:
	char a, b;
public:
	Zbroj() { cout << "Pozvan ekstra fancy constructor" << endl; };
	void set_a(char a) { this->a = a; }
	void set_b(char b) { this->b = b; }
	char get_a() { return a; };
	char get_b() { return b; };
	char zbroji(char a, char b) {
		if (a >= 97 && b >= 97)
			return a + b - 'a' + 1;
		if (a >= 65 && b >= 65)
			return a + b - 'A' + 1;
		if (a >= 48 && b >= 48)
			return a + b - '0';
	}
};
#endif