#include <iostream>
#include "Zbroj.h"
#include "Stack.h"
using namespace std;
template <typename T>
T zbroj(T x, T y)
{
	T rez;
	rez = x + y;
	return rez;
}
template<>
char zbroj(char x, char y)
{
	char rez;
	rez=x + y-'a'+1;
	return rez;
}
int main(void)
{
	Zbroj<int> klasa;
	Zbroj<char> klasa2;
	Stack<char> stack(3);
	Stack<char> stackmaxsizetest(1005);
	Stack<char> stackic;
	stackic.getSize();

	cout<<"Testing size: " << stackmaxsizetest.getSize() << endl;
	stack.push('A');
	cout << "Stack char" << stack.pop() << endl;
	Stack<int> stackint(3);
	stackint.push(2);
	cout << "Stack int" << stackint.pop() << endl;
	stackint.pop();
	cout << "Size is: " <<stack.getSize() << endl;
	stackint.push(2);
	stackint.push(2);
	stackint.push(2);
	stackint.push(2);
	stack.getSize();
	stack.~Stack();
	stackint.~Stack();
	klasa2.set_a('F');
	klasa2.set_b('E');
	cout << klasa2.zbroji(klasa2.get_a(), klasa2.get_b()) << endl;
	klasa.set_a(10);
	klasa.set_b(20);
	cout << klasa.zbroji(klasa.get_a(), klasa.get_b()) << endl;
	int a = 3;
	int b = 4;
	float d = 4.5;
	float e = 5.55;
	float f = zbroj(d, e);
	int c = zbroj(a, b);
	char x = '2';
	char y = '4';
	char z = zbroj(x, y);
	cout << c << " " << f << " " << z;
	cin >> c;
}