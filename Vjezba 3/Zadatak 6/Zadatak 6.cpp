//Korisnik unosi prirodni broj N, nakon cega unosi N stringova u vector(duljina svakog
//stringa mora biti manja od 20 - provjeriti unos, dozvoljeni su samo brojevi i velika slova
//engleske abecede).Ukoliko string sadrzava slova i brojeve, onda ga treba trasnformirati
//na nacin da se svaki podstring od 2 ili vise istih znakova zamijeni brojem ponavljanja(AAA = 3A).S druge strane, ukoliko string sadrzava samo slova onda ga treba
//transformirati suprotno prethodno opisanoj transformaciji(2F = FF).

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

vector<string> funkcija(vector<string> vektor, int n)
{
	int i = 0, j = 0, flag=0, count = 0;
	string count_s,newstr="                    ";
	while (i < n)
	{
		flag = 0;
		string str;
		cout << "unesite string\n";
		cin.ignore(1000, '\n');
		getline(cin,str);
		j = 0;
		while (str[j] != '\0')
		{
			if (isalpha(str[j])==0 && isdigit(str[j])==0 || j>=19 || isalpha(str[j]) != 0  && isupper(str[j])==0)
			{
				flag = 1;
				cout << "error\n";
				break;
			}
			j++;
		}
		j--;
		if(flag==0)
		{
			if (isalpha(str[j]) == 0)
			{
				cout << "zadnji znak ne smije biti broj\n";
			}
			else
			{
				vektor.push_back(str);
				i++;
			}
		}
		

	}
	i = 0;
	j = 0;
	//flag=0 za samo slova, 1 za slova + brojevi
	while (i < n)
	{
		newstr = "                    ";
		j = 0;
		while (vektor.at(i)[j] != '\0')
		{
			flag = 0;
			if (isalpha(vektor.at(i)[j]) == 0)
			{
				flag = 1;
				break;
			}
			j++;
		}
		j = 0;
		if (flag == 0)
		{
			count = 0;
			j = 0;
			while (vektor.at(i)[j] != '\0')
			{
				if (vektor.at(i)[j+1] != '\0' && vektor.at(i)[j] == vektor.at(i)[j+1])
				{
					count++;
				}
				else
				{
					if (count > 0)
					{
						count_s = to_string(count + 1);
						vektor.at(i).erase(j - count, count);
						j = j - count;
						vektor.at(i).insert(j, count_s);
						j++;
						count = 0;
					}

				}
				j++;
			}
			i++;
			
		}
		if (flag == 1)
		{
			int poz = 0;
			int k = 0;
			j = 0;
			while (vektor.at(i)[j] != '\0')
			{
				count = 0;
				

				while (isdigit(vektor.at(i)[j]) != 0)
				{
					int broj = vektor.at(i)[j] -'0';
					count = count*10 + broj;
					j++;
				}
				for (k = poz; k < count+poz-1; k++)
				{
					newstr[k] = vektor.at(i)[j];
				}
				newstr[k] = vektor.at(i)[j];
				k++;
				poz =k;
				j++;
			}
			newstr.erase(newstr.begin()+poz, newstr.end());
			vektor.at(i)=newstr;
			i++;
		}
	}
	return vektor;
}
int main()
{
	int n;
	cout << "unesite broj stringova\n";
	cin >> n;
	vector <string> vektor;
	vektor=funkcija(vektor, n);
	for (int i = 0; i < vektor.size(); i++)
	{
		cout << "vektor.at(" << i << ") je ";
		cout << vektor.at(i) << endl;
	}
	return 0;
}

