#pragma once
#ifndef VEC3_H
#define VEC3_H
#include <iostream>
using namespace std;
namespace OOP
{
class Vec3
{
	double x, y, z;
	static int br;
public:
	Vec3(){ x = 0,y = 0,z = 0,br++,cout << "br++" << endl; }
	~Vec3(){ cout << "delete" << endl; }
	friend istream& operator>>(istream&, Vec3& v);
	friend ostream& operator<<(ostream& os, const Vec3& v);
	Vec3& operator=(const Vec3& other);
	Vec3(const Vec3& other); 
	friend Vec3 operator+(const Vec3& a, const Vec3& b);
	friend Vec3 operator-(const Vec3& a, const Vec3& b);
	Vec3& operator/(int n);
	Vec3& operator*(int n);
	Vec3& operator+=(const Vec3& a);
	Vec3& operator-=(const Vec3& a);
	Vec3& operator*=(int n); 
	Vec3& operator/=(int n);
	friend bool operator==(const Vec3& a, const Vec3& b);
	friend bool operator!=(const Vec3& a, const Vec3& b);
	friend double operator*(const Vec3& a, const Vec3& b);
	double operator[](int n);
	double& getx() {return x;}
	double& gety() {return y;}
	double& getz() {return z;}
	static int getbr() {return br;}
};
Vec3& normalizacija(Vec3& n);
}
#endif