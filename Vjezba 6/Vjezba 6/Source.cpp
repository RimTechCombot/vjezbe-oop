#include <iostream>
#include <vector>
#include "Vec3.h"
#include <math.h>
using namespace std;
using namespace OOP;
int main(void)
{

	int x;
	char c='x';
	Vec3 vektor1, vektor2;
	cin >> vektor1;
	vektor2 = vektor1;
	cout << vektor1;
	cout << vektor2;
	vektor2 = vektor1 + vektor1;
	cout << vektor2;
	vektor2 = vektor2 - vektor1;
	cout << vektor2;
	vektor2 = vektor2 * 3;
	cout << vektor2;
	vektor2 = vektor2 / 5;
	cout << vektor2;
	vektor2 += vektor1;
	cout << vektor2;
	vektor2 -= vektor1;
	cout << vektor2;
	vektor2 *= 2;
	cout << vektor2;
	vektor2 /= 3;
	cout << vektor2;
	cout << (vektor1 == vektor2) << endl;
	cout << (vektor1 != vektor2) << endl;
	cout << vektor1 * vektor2 << endl;
	cout << normalizacija(vektor1) << endl;
	cout << OOP::Vec3::getbr() << endl;
	if (c == 'x')
		cout << "[]" << vektor2[0] << endl;
	else if (c == 'y')
		cout << "[]" << vektor2[1] << endl;
	else
		cout << "[]" << vektor2[2] << endl;
	cin >> x;
	return 0;
}