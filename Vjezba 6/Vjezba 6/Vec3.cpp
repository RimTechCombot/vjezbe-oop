#include "Vec3.h"
#include <iostream>
#include <vector>
#include <math.h>
using namespace std;
namespace OOP
{
	int OOP::Vec3::br = 0;
	istream& operator>>(istream & is, Vec3 & v) {
		is >> v.x >> v.y >> v.z;
		return is;
	}
	ostream& operator<<(ostream & os, const Vec3 & v) {
		os << '(' << v.x << ',' << v.y << ',' << v.z << ')';
		return os;	}	Vec3& Vec3::operator=(const Vec3& a)	{		x=a.x;		y=a.y;		z=a.z;		return *this;	}	Vec3::Vec3(const Vec3& other) {		*this = other;	}	Vec3 operator+(const Vec3& a, const Vec3& b)	{		Vec3 novi;		novi.x = a.x + b.x;		novi.y = a.y + b.y;		novi.z = a.z + b.z;		return novi;	}	Vec3 operator-(const Vec3 & a, const Vec3 & b)	{		Vec3 novi;		novi.x = a.x - b.x;		novi.y = a.y - b.y;		novi.z = a.z - b.z;		return novi;	}	Vec3& Vec3::operator/(int n)	{
		x = x / n;		y = y / n;		z = z / n;		return *this;	}	Vec3& Vec3::operator*(int n)	{
		x = x * n;		y = y * n;		z = z * n;		return *this;	}	Vec3& Vec3::operator+=(const Vec3& a)	{
		x += a.x;
		y += a.y;
		z += a.z;
		return *this;	}	Vec3& Vec3::operator-=(const Vec3& a)	{
		x -= a.x;
		y -= a.y;
		z -= a.z;
		return *this;	}	Vec3& Vec3::operator*=(int n)	{
		x *= n;
		y *= n;
		z *= n;
		return *this;	}	Vec3& Vec3::operator/=(int n)	{
		x /= n;
		y /= n;
		z /= n;
		return *this;	}	bool operator==(const Vec3& a, const Vec3& b)	{
		return (a.x == b.x && a.y == b.y && a.z == b.z)	}	bool operator!=(const Vec3& a, const Vec3& b)	{
		return (a.x != b.x || a.y != b.y || a.z != b.z)	}	double operator*(const Vec3& a, const Vec3& b)	{		return a.x * b.x + a.y * b.y + a.z * b.z;	}	double Vec3::operator[](int n)
	{
		vector<double> vektor;
		vektor.push_back(x);
		vektor.push_back(y);
		vektor.push_back(z);
		return vektor.at(n);
	}
	Vec3& normalizacija(Vec3& a)
	{
		double norm = sqrt(a.getx()*a.getx() + a.gety()*a.gety() + a.getz()*a.getz());
		a.getx() = a.getx() / norm;
		a.gety() = a.gety() / norm;
		a.getz() = a.getz() / norm;
		return a;
	}
}