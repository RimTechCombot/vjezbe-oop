#pragma once
#ifndef GAME_H
#define GAME_H
#include <iostream>
using namespace std;
class Game
{
private:
	char Board[3][3] = {'1','2','3','4','5','6','7','8','9'};
	//int BoardFlags[3][3] = { 0,0,0,0,0,0,0,0,0 };
	char Player;
	int flag, PlacementFlag, counter;
	int P1Score = 0;
	int P2Score = 0;
public:
	Game() { flag = 0, PlacementFlag = -1, counter=0; };
	void Draw();
	void Input();
	//void PlaceX(int choice);
	//void PlaceO(int choice);
	void Place(int choice);
	void ClearBoard();
	//int CheckRow();
	//int CheckColumn();
	//int CheckDiagonal();
	int CheckWin();
	int Replay();
	void Choice();
	void Toggle();
	//void ToggleMessage();
	void Message();
	int GameState();
	void SetPlayer(char choice) { Player = choice; }
	char GetPlayer() { return Player; };
	int GetP1Score() { return P1Score; };
	int GetP2Score() { return P2Score; };
	void IncrementP2Score() { P2Score++; };
	void IncrementP1Score() { P1Score++; };
	int GetFlag() { return flag; };
	//void SetBoardFlags(int value, int i,int j) { BoardFlags[i][j] = value; };
	//int GetBoardFlags(int i, int j) { return BoardFlags[i][j]; };
	char GetBoard(int i, int j) { return Board[i][j]; };
	void SetBoard(char value, int i, int j) { Board[i][j] = value; }
	int GetPlacementFlag() { return PlacementFlag; };
	int GetCounter() { return counter; };
	void SetCounter(int flagius) { counter = flagius; };
	void IncrementCounter() { counter++; };
	void SetPlacementFlag(int flagius) { PlacementFlag = flagius; };
	void SetFlag(int flagius) { flag = flagius; };
};


#endif