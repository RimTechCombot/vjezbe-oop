#include <iostream>
#include "Game.h"
#include <ctype.h>
using namespace std;
void Game::Draw()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << GetBoard(i,j);
			if (j != 2)
				cout << " | ";
		}
		if (i != 2)
		{
			cout << endl;
			cout << "__________";
		}
		cout << endl;
	}
}
void Game::Input()
{
	while(1)
	{ 
		int choice;
		Draw();
		Choice();
		while (GameState() == 1)
		{
				while (GetPlacementFlag() == -1)
				{
					Message();
					cin >> choice;
					while (choice < 0 || choice >9)
					{
						cout << "Field doesn't exist, please pick from 1 to 9" << endl;
						cin >> choice;
					}
					Place(choice);
				}
				SetPlacementFlag(-1);
				system("cls");
				Draw();
				if (GetCounter() != 9)
				{
					Toggle();
				}

		}
		cout << "Player 1 score: " << GetP1Score() << endl << "Player 2 score: " << GetP2Score() << endl;
		if(Replay()==0)
			break;
		system("cls");
	}
}
void Game::Choice()
{
	char choice='a';
	cout << "Player 1, choose X or O" << endl;
	cin >> choice;
	while (choice != 'x' && choice != 'X' && choice != 'o' && choice != 'O')
	{
		cout << "Incorrect input, choose X or O" << endl;
		cin >> choice;
	}
	SetPlayer(toupper(choice));
}
void Game::Toggle()
{
	if (CheckWin() == 0)
	{
		if (GetPlayer() == 'X')
			SetPlayer('O');
		else
			SetPlayer('X');
		if (GetFlag() == 0)
		{
			cout << "Player 2's turn (" << Player << ")" << endl;
			SetFlag(1);
		}
		else
		{
			cout << "Player 1's turn (" << Player << ")" << endl;
			SetFlag(0);
		}
	}

}
void Game::Place(int choice)
{
	int i, j;
	j = (choice - 1) % 3;
	i = (choice - 1) / 3;
	if (GetBoard(i, j) != 'X' && GetBoard(i, j) != 'O')
	{
		SetBoard(Player, i, j);
		//SetBoardFlags(1, i, j);
		SetPlacementFlag(0);
		IncrementCounter();
	}
	else
	{
		cout << "Field in use, please pick another" << endl;
		SetPlacementFlag(-1);
	}
}
void Game::Message()
{
	cout << "Choose the field to place the " << Player <<" (1-9)" << endl;
}
int Game::GameState()
{
	if (GetCounter() == 9 && CheckWin() == 0)
	{
		cout << "It's a draw" << endl;
		return 0;
	}
	if (CheckWin() == 1)
	{
		if (GetCounter() % 2 == 0) {cout << "Player two (" << Player << ") wins!" << endl; IncrementP2Score();}
		else { cout << "Player one (" << Player << ") wins!" << endl; IncrementP1Score(); }
		return 0;
	}
	return 1;

}
int Game::CheckWin()
{
	int i = 0;
	for (i = 0; i < 3; i++)
		if (GetBoard(i, 0) == GetBoard(i, 1) && GetBoard(i, 1) == GetBoard(i, 2))
			return 1;
	int j = 0;
	for (j = 0; j < 3; j++)
		if (GetBoard(0, j) == GetBoard(1, j) && GetBoard(1, j) == GetBoard(2, j))
			return 1;
	if (GetBoard(0, 0) == GetBoard(1, 1) && GetBoard(1, 1) == GetBoard(2, 2) ||
		GetBoard(0, 2) == GetBoard(1, 1) && GetBoard(1, 1) == GetBoard(2, 0))
		return 1;
	return 0;
}
int Game::Replay()
{
	char choice;
	cout << "Replay? (Y/N)" << endl;
	cin >> choice;
	while (choice != 'y' && choice != 'Y' && choice != 'n' && choice != 'N')
	{
		cout << "Incorrect input,please write Y or N";
	}
	if (toupper(choice) == 'Y')
	{ 
		SetCounter(0);
		ClearBoard();
		Draw();
		return 1;
	}
	else
	{
		if (GetP1Score() > GetP2Score())
		{
			cout << "Player 1 Wins! GG WP!" << endl;
		}
		else if (GetP2Score() > GetP1Score())
		{
			cout << "Player 2 Wins! GG WP!" << endl;
		}
		else
		{
			cout << "Draw! GG WP!" << endl;
		}
		return 0;
	}
}
void Game::ClearBoard()
{
	char temp = '1';
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			Board[i][j] = temp++;
}