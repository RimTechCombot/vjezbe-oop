using namespace std;
#include <iostream>
#include <string>
#include <sstream>
#define noop
struct student {
	string ID;
	string ime;
	string spol;
	int kviz1 = 0;
	int kviz2 = 0;
	int midterm = 0;
	int finalscore = 0;
	int totalscore = 0;
};
int prikaz(struct student, int = 0, int = 19);
int search(struct student, string = "");

int unosocjene()
{
	int ocjena;
	string str;
	cout << "Unesite ocjenu ili X za izlaz\n";
	getline(cin, str);
	while (str != "1" && str != "2" && str != "3" && str != "4" && str != "5" && str != "X")
	{
		cout << "Neispravan unos";
		getline(cin, str);
	}
	if (str == "X")
		return 0;
	stringstream temp(str);
	temp >> ocjena;
	return ocjena;
}
int unos(struct student niz[])
{
	string str;
	int i;
	int j;
	int flag = 0;
	for (i = 0; i < 19; i++)
	{
		while (niz[i].ID != "")
			i++;
		if (i > 19)
		{
			cout << "nema vise prosotra, i je " << i << "\n";
			return 0;
		}
		cout << "Unesite ID studenta ili X za izlaz\n";
		getline(cin, str);
		if (str == "X")
			return 1;
		/*
		for(i=0;i<20;i++)
			for(j=0;j<20;j++)
				if(niz[i].ID != "" && niz[i].ID==niz[j].ID && i!=j)
				{
					while (niz[i].ID == niz[j].ID)
					{
						cout << "ID je vec iskoristen, unesite novi";
						getline(cin, str);
					}
				}
		i = 0;
		while (niz[i].ID != "")
			i++;
		*/

		cout << "i je trenutno " << i << "\n";
		for (j = 0; j < i; j++)
			while (str == niz[j].ID)
			{
				cout << "ID je vec iskoristen, unesite novi";
				getline(cin, str);
			}
		niz[i].ID = str;
		cout << "Unesite ime studenta ili X za izlaz\n";
		getline(cin, str);
		if (str == "X")
			return 1;
		niz[i].ime = str;
		cout << "Unesite spol studenta ili X za izlaz\n";
		getline(cin, str);
		while (str != "M" && str != "F" && str != "X")
		{
			cout << "Spol moze biti M ili F\n";
			getline(cin, str);
		}
		if (str == "X")
			return 1;
		niz[i].spol = str;
		cout << "Sada dolaze rezultati studenta, ima li student vec neke rezultate? (Y/N)\n";
		getline(cin, str);
		while (str != "Y" && str != "N")
		{
			cout << "Ponovite upis";
			getline(cin, str);
		}
		if (str == "N")
			return 1;
		else
		{
			while (true)
			{
				if (str == "X")
				{
					cout << "Izaci iz upisa?(Y/N)\n";
					getline(cin, str);
					while (str != "Y" && str != "N")
					{
						cout << "Neispravan unos\n";
						getline(cin, str);
					}
					if (str == "Y")
						return 0;
				}
				cout << "Sto zelite unijeti? Kviz1, Kviz2, Midterm, Final (X za izlaz)\n";
				getline(cin, str);
				while (str != "Kviz1" && str != "Kviz2" && str != "Midterm" && str != "Final" && str != "X")
				{
					cout << "Neispravan unos\n";
					getline(cin, str);
				}
				if (str == "Kviz1")
				{
					cout << "Unesite ocjenu za prvi kviz (X za izlaz)\n";
					getline(cin, str);
					while (str != "1" && str != "2" && str != "3" && str != "4" && str != "5" && str != "X")
					{
						cout << "Neispravan unos\n";
						getline(cin, str);
					}
					if (str == "X")
						continue;
					else
					{
						stringstream temp(str);
						temp >> niz[i].kviz1;
					}

				}
				else if (str == "Kviz2")
				{
					cout << "Unesite ocjenu za drugi kviz (X za izlaz)\n";
					getline(cin, str);
					while (str != "1" && str != "2" && str != "3" && str != "4" && str != "5" && str != "X")
					{
						cout << "Neispravan unos\n";
						getline(cin, str);
					}
					if (str == "X")
						continue;
					else
					{
						stringstream temp(str);
						temp >> niz[i].kviz2;
					}
				}
				else if (str == "Midterm")
				{
					cout << "Unesite ocjenu za midterm (X za izlaz)\n";
					getline(cin, str);
					while (str != "1" && str != "2" && str != "3" && str != "4" && str != "5" && str != "X")
					{
						cout << "Neispravan unos\n";
						getline(cin, str);
					}
					if (str == "X")
						continue;
					else
					{
						stringstream temp(str);
						temp >> niz[i].midterm;
					}
				}
				else if (str == "Final")
				{
					if (niz[i].kviz1 == 0 || niz[i].kviz2 == 0 || niz[i].kviz2 == 0)
					{
						cout << "Nemoguce je upisati finalscore bez upisanih drugih ocjena\n";
						flag = 1;
					}
					if (flag == 1)
					{
						noop;
						flag = 0;
					}
					else
					{
						cout << "Unesite ocjenu za midterm (X za izlaz)\n";
						getline(cin, str);
						while (str != "1" && str != "2" && str != "3" && str != "4" && str != "5" && str != "X")
						{
							cout << "Neispravan unos\n";
							getline(cin, str);
						}
						if (str == "X")
							continue;
						else
						{
							stringstream temp(str);
							temp >> niz[i].finalscore;
						}
					}

				}
				if (niz[i].kviz1 != 0 && niz[i].kviz2 != 0 && niz[i].midterm != 0 && niz[i].finalscore != 0)
				{
					niz[i].totalscore = niz[i].kviz1 + niz[i].kviz2 + niz[i].midterm + niz[i].finalscore;
					cout << "Generiran je totalscore za studenta! Izlaz? (Y/N)\n";
					getline(cin, str);
					while (str != "Y" && str != "N")
					{
						cout << "Neispravan unos\n";
						getline(cin, str);
					}
					if (str == "Y")
						return 1;
					else
						noop;
				}
			}
		}

	}
}
int prikaz(struct student niz[], int j, int k)
{
	int i;
	for (i = j; i < k; i++)
		if (niz[i].ID != "")
		{
			cout << "ID: " << niz[i].ID << "\n" << "Ime studenta: " << niz[i].ime << "\n" << "Spol: " << niz[i].spol << "\n" << "Kviz1: " << niz[i].kviz1 << "\n";
			cout << "Kviz12 " << niz[i].kviz2 << "\n" << "Midterm: " << niz[i].midterm << "\n" << "Final: " << niz[i].finalscore << "\n" << "Total: " << niz[i].totalscore << "\n";
		}
	return 0;
}
int search(struct student niz[], string str)
{
	int i = 0, j, indeks = 0;
	int min, max;
	if (str == "X")
		return -1;
	if (str != "")
	{
		while (niz[i].ID != str)
		{
			i++;
			if (i == 19)
			{
				cout << "Nema studenta sa tim ID-em\n";
				return -1;
			}
		}
		return i;
	}
	if (str == "")
	{
		cout << "Kriterij pretrage - minimalni broj bodova, maksimalni broj bodova, ili ID? (Min,Max,ID,X za izlaz)\n";
		getline(cin, str);
	}
	while (str != "Min" && str != "Max" && str != "ID" && str != "X")
	{
		cout << "Neispravan unos\n";
		getline(cin, str);
	}
	if (str == "Min")
	{
		while (niz[i].ID == "" || niz[i].totalscore == 0)
		{
			i++;
			if (i == 19)
			{
				cout << "Nema unesenih studenata\n";
				return -1;
			}
		}
		min = niz[i].totalscore;
		indeks = i;
		for (j = i + 1; j < 19; j++)
			if (niz[j].totalscore < min && niz[j].totalscore>0)
			{
				min = niz[j].totalscore;
				indeks = j;
			}
		prikaz(niz, indeks, indeks + 1);
		return 0;
	}
	if (str == "Max")
	{
		while (niz[i].ID == "" || niz[i].totalscore == 0)
		{
			i++;
			if (i == 19)
			{
				cout << "Nema unesenih studenata\n";
				return -1;
			}
		}
		max = niz[i].totalscore;
		indeks = i;
		for (j = i + 1; j < 19; j++)
			if (niz[j].totalscore > max && niz[j].totalscore > 0)
			{
				max = niz[j].totalscore;
				indeks = j;
			}
		prikaz(niz, indeks, indeks + 1);
		return 0;
	}
	if (str == "ID")
	{
		cout << "Unesite ID trazenog studenta";
		getline(cin, str);
		while (niz[i].ID != str)
		{
			i++;
			if (i == 19)
			{
				cout << "Nema studenta sa tim ID-em\n";
				return -1;
			}
		}
		prikaz(niz, i, i + 1);
		return 0;
	}
}
int sort(struct student niz[])
{
	int i, j;
	struct student temp;
	for (i = 0; i < 18; i++)
		for (j = i + 1; j < 19; j++)
		{
			if (niz[i].totalscore < niz[j].totalscore)
			{
				temp = niz[i];
				niz[i] = niz[j];
				niz[j] = temp;
			}
		}
	return 0;
}
int prosjek(struct student niz[])
{
	string str;
	int i = 0;
	double convert;
	double prosjek;
	cout << "Unesite ID studenta ili X za izlaz\n";
	getline(cin, str);
	i = search(niz, str);
	if (i == -1)
		return 0;
	convert = niz[i].kviz1;
	prosjek = (convert + niz[i].kviz2 + niz[i].midterm + niz[i].finalscore) / 4;
	cout << "Prosjek tog studenta je: " << prosjek << "\n";
	return 0;
}
int del(struct student niz[])
{
	int i = 0, j = 18;
	string str;
	cout << "Unesite ID za pronalazenje studenta za brisanje ili X za izlaz\n";
	getline(cin, str);
	struct student temp;
	i = search(niz, str);
	if (i == -1)
		return 0;
	while (niz[j].ID == "" && j >= i)
		j--;
	temp = niz[j];
	niz[j] = niz[i];
	niz[i] = temp;
	niz[j].ID = "";
	return 0;
}
int azuriraj(struct student niz[])
{
	int i = 0;
	int ocjena;
	string str;
	int temp;
	cout << "Unesite ID studenta kojeg zelite azurirati ili X za izlaz\n";
	getline(cin, str);
	i = search(niz, str);
	if (i == -1)
		return 0;
	while (true)
	{
		cout << "Sto zelite azurirati,Podatke ili Rezultate?\n";
		getline(cin, str);
		while (str != "Podatke" && str != "Rezultate" && str != "X")
		{
			cout << "Neispravan unos\n";
			getline(cin, str);
		}
		if (str == "Podatke")
		{
			cout << "Ime ili Spol (X za izlaz)\n";
			getline(cin, str);
			while (str != "Ime" && str != "Spol" && str != "X")
			{
				cout << "Neispravan Unos\n";
				getline(cin, str);
			}
			if (str == "Ime")
				niz[i].ime = str;
			else if (str == "Spol")
				niz[i].spol = str;
			else
				noop;
		}
		else if (str == "Rezultate")
		{
			cout << "Kviz1, Kviz2, Midterm, Final?(X za izlaz)\n";
			getline(cin, str);
			while (str != "Kviz1" && str != "Kviz2" && str != "Midterm" && str != "Final" && str != "X")
			{
				cout << "Neispravan Unos\n";
				getline(cin, str);
			}
			if (str == "Kviz1")
			{
				ocjena = unosocjene();
				if (ocjena == 0)
					noop;
				else
					niz[i].kviz1 = ocjena;

			}
			else if (str == "Kviz2")
			{
				ocjena = unosocjene();
				if (ocjena == 0)
					noop;
				else
					niz[i].kviz2 = ocjena;
			}
			else if (str == "Midterm")
			{
				ocjena = unosocjene();
				if (ocjena == 0)
					noop;
				else
					niz[i].midterm = ocjena;
			}
			else if (str == "Final")
			{
				ocjena = unosocjene();
				if (ocjena == 0)
					noop;
				else
					niz[i].finalscore = ocjena;
			}
			else
				noop;
		}
		else
			return 0;
		if (niz[i].kviz1 != 0 && niz[i].kviz2 != 0 && niz[i].midterm != 0 && niz[i].finalscore != 0) //Problem
		{
			niz[i].totalscore = niz[i].kviz1 + niz[i].kviz2 + niz[i].midterm + niz[i].finalscore;
			cout << "Generiran je novi total score!\n";
		}
	}
	return 0;
}

int main()
{

	string choice;
	student *niz = new student[19];
	cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
	getline(cin, choice);
	while (true)
	{
		if (choice == "A")
		{
			azuriraj(niz);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "U")
		{
			unos(niz);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "P")
		{
			prikaz(niz, 0, 19);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "PR")
		{
			search(niz, "");
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "S")
		{
			sort(niz);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "I")
		{
			prosjek(niz);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "DEL")
		{
			del(niz);
			cout << "Azuriraj, Unesi, Prikaz, Pretraga, Izracun, Sortiraj, Izbrisi (A,U,P,PR,I,S,DEL, ili X za prekinuti rad)\n";
			getline(cin, choice);
		}
		else if (choice == "X")
			break;
		else
		{
			cout << "Neispravan unos, ponovite zahtjev\n";
			getline(cin, choice);
		}

	}
}