#include <iostream>
#include <new>
/*int main()
{
	int a, b, s;
	std::cout << "unesi dva broja" << std::endl;
	std::cin >> a >> b;
	s = a + b;
	std::cout << "suma je " << s << std::endl;
}
*/
using namespace std;
int main()
{
	int brojponavljanja = 0;
	int n, i;
	int temp;
	cout << "unesi velicinu niza" << endl;
	cin >> n;
	int *niz = new int[n];
	for (i = 0; i < n; i++)
	{
		cout << "unesi " << i << ". element niza" << endl;
		cin >> niz[i];
	}
	for (i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n; j++)
			if (niz[i] > niz[j])
			{
				temp = niz[i];
				niz[i] = niz[j];
				niz[j] = temp;
			}
	}
	for (i = 0; i < n - 1; i++)
	{
		if (niz[i] != niz[i + 1])
		{
			cout << niz[i] << " se ponovio " << brojponavljanja << " puta" << endl;
			brojponavljanja = 0;
		}
		else
			brojponavljanja++;
	}
	cout << niz[i] << " se ponovio " << brojponavljanja << " puta" << endl;
	for (i = 0; i < n; i++)
	{
		cout << niz[i];
	}
	delete[] niz;
}
