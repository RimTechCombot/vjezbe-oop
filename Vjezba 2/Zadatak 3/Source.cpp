// Zadatak 3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

struct vector
{
	int*niz;
	int fiz, log;
	void vector_new(int n)
	{
		niz = new int[n];
		fiz = n;
		log = 0;
	}
	void vector_delete()
	{
		delete[] niz;
	}
	void vector_push_back(int a)
	{
		if (fiz == log)
		{
			niz = (int*)realloc(niz, fiz * 2 * sizeof(int));
		}
		niz[log] = a;
		log++;
	}
	void vector_pop_back()
	{
		if (log == 0)
			return;
		niz = (int*)realloc(niz, (fiz-1)* sizeof(int));
		fiz--;
		log--;
		return;
	}
	int& vector_front()
	{
		if (log == 0)
			log++;
		return niz[0];
	}
	int& vector_back()
	{
		if (log == 0)
			log++;
		return niz[log-1];
	}
};

int main()
{
	int n, broj, i;
	vector a;
	cout << "Unesite n\n";
	cin >> n;
	a.vector_new(n);
	for (i = 0; i < n; i++)
	{
		cout << "Unesite broj u vektor\n";
		cin >> broj;
		a.vector_push_back(broj);
	}
	for (i = 0; i < n; i++)
	{
		a.vector_pop_back();
	}
	a.vector_push_back(2);
	a.vector_front() = 20;
	a.vector_back() = 30;
	cout << a.log << "\n";
	cout << a.vector_back() << "\n";
	cout << a.vector_front() << "\n";
	cout << a.fiz;
	if(a.fiz!=0)
		a.vector_delete();
	return 0;
}

