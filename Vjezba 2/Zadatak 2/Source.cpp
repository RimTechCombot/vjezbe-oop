/*
Napisite funkciju koja odvaja parne i neparne brojeve u nizu tako da se prvo pojavljuju
parni brojevi, a zatim neparni brojevi. Memoriju za niz alocirati dinamicki. Primjer
ulaza i izlaza:
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27
*/


#include <iostream>
#include <new>
using namespace std;
int* funkcija(int n)
{
	int i,j, temp;
	int *niz = new int[n];
	for (i = 0; i < n; i++)
	{
		cout << "Unesite " << i << ". element niza\n";
		cin >> niz[i];
	}
	for(i=0;i<n-1;i++)
		for(j=i+1;j<n;j++)
			if (niz[i] % 2 == 1 && niz[j] % 2 == 0)
			{
				temp = niz[i];
				niz[i] = niz[j];
				niz[j] = temp;
			}
	return niz;
}

int main()
{
	int n, i;
	cout << "Unesite velicinu niza\n";
	cin >> n;
	int *niz=funkcija(n);
	for (i = 0; i < n; i++)
		cout << niz[i] << " ";
	delete[] niz;
	return 0;
}
