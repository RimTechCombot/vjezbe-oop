
/*
Napisati funkciju koja pronalazi element koji nedostaje u cjelobrojnom rasponu 1-9.
Memoriju za niz alocirati dinamicki, korisnik popunjava niz, a funkcija prepoznaje i
vraca modificirani sortirani niz s brojem koji nedostaje. U main funkciji ispisati dobiveni
niz i osloboditi memoriju
*/
#include <iostream>
#include <new>
using namespace std;

int* funkcija(int n)
{
	int loop = 1;
	while (loop = 1)
	{
		int i = 0, j, indeks, temp, p, brojac = 1, flag = 0, k = 0, element;
		int *niz = new int[n];
		while (i < n)
		{
			cout << "unesite " << i << ". element niza\n";
			cin >> p;
			if (p <= 0 || p >= 10)
			{
				cout << "Uneseni element nije iz raspona\n";

			}
			else
			{
				niz[i] = p;
				i++;
			}


		}
		for (i = 0; i < n - 1; i++)
			for (j = i + 1; j < n; j++)
				if (niz[i] > niz[j])
				{
					temp = niz[i];
					niz[i] = niz[j];
					niz[j] = temp;
				}
		i = 0;
		while (i != n)
		{
			if (niz[i] == brojac)
			{
				while (niz[i] == brojac && i < n)
				{
					cout << "Brojac je " << brojac << " Niz[i] je " << niz[i] << "\n";
					i++;
				}
				brojac++;
			}
			else
			{
				cout << "Brojac je " << brojac << " Niz[i] je " << niz[i] << " FLAG UVJET\n";
				flag++;
				indeks = i;
				element = brojac;
				brojac++;
				loop = 0;

			}
		}
		if (flag == 0 && brojac == 9)
		{
			indeks = i;
			element = brojac;
			loop = 0;
		}
		else if (flag > 1 || (flag <= 1 && brojac <= 8))
		{
			cout << flag;
			cout << "Neispravni ulazni niz, ne sadrzi vise elemenata raspona 1-9\n";
			loop = 1;
		}
		else if (flag == 0 && brojac == 10)
		{
			cout << "Neispravni ulazni niz, sadrzi sve elemente raspona 1-9\n";
			loop = 1;
		}
		i = 0;
		if (loop == 0)
		{
			int *noviniz = new int[n + 1];
			while (k < n + 1)
			{
				if (i == indeks)
				{
					noviniz[k] = element;
					k++;
				}
				noviniz[k] = niz[i];
				i++;
				k++;
			}
			delete[] niz;
			return noviniz;
		}
	}
}

int main()
{
	int n, i;
	cout << "Unesite velicinu niza\n";
	cin >> n;
	while (n < 9)
	{
		cout << "velicina niza ne smije biti manja od 9";
		cin >> n;
	}
	int *niz = funkcija(n);
	for (i = 0; i < n + 1; i++)
	{
		cout << niz[i];
	}
	delete[] niz;
	return 0;
}

