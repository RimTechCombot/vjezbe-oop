#pragma once
#ifndef ERRORLOG_H
#define ERRORLOG_H
#include <iostream>
#include <string>
using namespace std;
class ErrorLog
{
public:
	virtual void PrintConsole() = 0;
	virtual string PrintLog() = 0;
};

#endif