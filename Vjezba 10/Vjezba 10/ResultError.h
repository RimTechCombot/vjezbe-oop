#pragma once
#ifndef RESULTERROR_H
#define RESULTERROR_H
#include "ErrorLog.h"
class ResultError : public ErrorLog
{
public:
	void PrintConsole() { std::cout << "Division by zero" << std::endl; };
	string PrintLog() { return "Division by zero"; };
};
#endif