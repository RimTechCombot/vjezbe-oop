#include<iostream>
#include<fstream>
#include<string>
#include "ErrorLog.h"
#include "ResultError.h"
#include "OperandError.h"
#include "OperatorError.h"
using namespace std;
int OperandInput()
{
	int number;
	cin >> number;
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
		throw OperandError();
	}
	return number;
}
int OperatorInput()
{
	char op;
	cin >> op;
	if (op!='+' && op!='-' && op!='/' && op!='*') throw OperatorError();
	return op;
}

int ResultOutput(int Op1, int Op2, char Operation)
{
	if (Operation == '+')
	{
		return Op1 + Op2;
	}
	else if (Operation == '-')
	{
		return Op1 - Op2;
	}
	else if (Operation == '*')
	{
		return Op1 * Op2;
	}
	else
	{
		if (Op2 == 0)
			throw(ResultError());

		else
			return Op1 / Op2;
	}
}
int main()
{
	int Op1, Op2;
	char Operation;
	while (1)
	{
		try
		{
			Op1 = OperandInput();
			Operation = OperatorInput();
			Op2 = OperandInput();
			cout << ResultOutput(Op1, Op2, Operation) << endl;
		}
		catch (ErrorLog& child)
		{
			ofstream FILE;
			FILE.open("Log.txt", ios::out | ios::app);
			FILE << child.PrintLog() << endl;
			FILE.close();
			child.PrintConsole();
		}

	}
}