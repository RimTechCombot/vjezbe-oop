#pragma once
#ifndef OPERANDERROR_H
#define OPERANDERROR_H
#include "ErrorLog.h"
class OperandError : public ErrorLog
{
public:
	void PrintConsole() { std::cout << "Input is not a valid operand" << std::endl; };
	string PrintLog() { return "Input is not a valid operand"; };

};
#endif
