#pragma once
#ifndef OPERATORERROR_H
#define OPERATORERROR_H
#include "ErrorLog.h"
class OperatorError : public ErrorLog 
{
public:
	void PrintConsole() { std::cout << "Input is not a valid operator" << std::endl; };
	string PrintLog() { return "Input is not a valid operator"; };
};
#endif